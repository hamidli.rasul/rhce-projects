#!/bin/bash

instance_name=$1

TOMCAT_BASE=/tomcat_base

INSTANCE_BASE=$TOMCAT_BASE/instances

CATALINA_HOME=$TOMCAT_BASE/apache-tomcat-9.0.73

CATALINA_BASE=$INSTANCE_BASE/$instance_name

export CATALINA_BASE CATALINA_HOME

$CATALINA_HOME/bin/startup.sh


